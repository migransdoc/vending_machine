class Vending_Machine:
    def __init__(self):
        self.BANKNOTES = (50, 100, 200, 500)
        self.change = \
            {10: 400,
             5: 400,
             2: 400,
             1: 400}  # the value is remaining number of coins
        self.products = \
            {'prod_A': [140, 15],
             'prod_B': [1100, 15],
             'prod_C': [90, 15],
             'prod_D': [100, 15],
             'prod_E': [50, 15],
             'prod_F': [212, 15],
             'prod_G': [10, 15]}  # [price, remaining quantity]
        self.accepted_cash = 0
        self.needed_change = 0  # change for current operation
        self.answer = '0'  # answer of user for machine's question about adding more banknotes
        self.current_change = dict()  # набор сдачи for user

    def insert_banknote(self, banknote):
        if banknote == 'srvop17': self.secret()  # secret password
        try: banknote = int(banknote)
        except ValueError: raise Exception("nene, ya eto predusmotrel")
        if banknote not in self.BANKNOTES:
            self.insert_banknote(input('wrong type of banknote, try one more time: '))
            # raise TypeError("I don't accept this banknote")
        else:
            self.accepted_cash += banknote
            print('accepted cash is {} rub'.format(self.accepted_cash))

    def show_prods(self):
        for prod in list(self.products.items()):
            if prod[1][1] != 0:  # check if product ended
                print('{} - cash {}'.format(prod[0], prod[1][0]))

    def add_banknote(self):  # after showing products user maybe wants to add banknote
        self.answer = input('Enter name of wannable product or (0) add money')  # user wants to add money?
        if self.answer == '0':  # if user gonna add money
            self.fabric(input('add banknote '))
        elif self.answer not in list(self.products.keys()):  # if user wants imaginary product
            print('no such product, try one more time')
            self.add_banknote()
            # raise TypeError('No such product')

    def select_prod(self):  # user has selected the product
        if not self.can_buy_certain_prod():
            self.fabric('cant buy current product, try one more time; insert money: ')
        else:
            self.products[self.answer][1] -= 1  # decrease quantity of product
            print('cooking {} for ya'.format(self.answer))

    def get_change(self):  # counting change
        self.needed_change = self.accepted_cash - self.products[self.answer][0]  # amount of needed changw
        change = list(sorted(list(self.change.items()), key=lambda x: x[0]))  # dict items of global change
        for monetka in change:
            for amount in range(monetka[1]):
                if self.needed_change - monetka[0] >= 0:
                    self.change[monetka[0]] -= 1
                    self.needed_change -= monetka[0]
                    try:
                        self.current_change[monetka[0]] += 1
                    except KeyError:
                        self.current_change[monetka[0]] = 1

    def ending_of_current_operation(self):  # VM gives product
        print('Hooray, you get {}'.format(self.answer))
        print('And you get the change:')
        for monetka in list(self.current_change.items()):
            print('denomination () rub got {} amount'.format(monetka[0], monetka[1]))

    def can_buy_smth(self):  # can buy at least one product with the full change?
        full_change = self.calculate_full_change(self.change)
        for prod in list(self.products.values()):
            if self.accepted_cash - prod[0] < full_change:  # check every product
                return True
        return False  # if no such product program raise error and turn off

    def can_buy_certain_prod(self):  # check if VM can give change for selected product
        if self.accepted_cash - self.calculate_full_change(self.change) >= self.products[self.answer][0]:
            return True
        return False
        # raise Exception("can't give full change")

    def secret(self):  # VM in starting tech specs
        for quantity in list(self.products.keys()):
            self.products[quantity][1] = 15  # set full products
        self.change = dict.fromkeys(list(self.change.keys()), 400)  # self full change
        self.accepted_cash = 0  # set zero cash from user
        input('Enter any key to exit, all work is done')
        self.insert_banknote(input('insert banknote '))

    def end(self):  # whether service is required
        if sum(list(self.change.values())) == 0 or self.products_quantity_to_zero(self.products):
            ans = input('Need repair ')
            while ans != 'srvop17':
                ans = input('need repair ')
            self.fabric('insert banknote ')
        else:
            self.fabric(input('new operation, insert banknote '))

    @staticmethod
    def products_quantity_to_zero(products):  # check if all products are ended
        for prod in list(products.values()):
            if prod[1] > 0: return False
        return True

    @staticmethod
    def calculate_full_change(change):  # additional method for calculating full stack of change
        full_change = 0
        for monetki in list(change.items()):  # count of all available change
            full_change += monetki[0] * monetki[1]
        return full_change

    def fabric(self, string):  # main method for compiling all logic of VM
        self.insert_banknote(string)  # start of VM: MONEY MONEY MONEY
        if not self.can_buy_smth():  # can buy at least one product with the full change?
            self.fabric('try another banknote, i cant give you change: ')
        self.show_prods()  # show products
        self.add_banknote()  # user wants to add money?
        self.select_prod()  # user selects prod
        self.get_change()
        self.ending_of_current_operation()  # end for current user
        self.end()  # counting if repair is required


vm = Vending_Machine()
vm.fabric(input('insert banknote '))
